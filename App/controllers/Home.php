<?php

namespace Controllers;

use App\helpers\Helper;
use App\services\SessionAuth;
use \Core\Controller;

class Home extends Controller

{

    public function index ()
    {
        if (SessionAuth::isLoggedIn()) {
            Helper::redirect('/user/personal');
        }
        $this->render('home', ['title' => 'Dev Communication']);

    }
    public function about ()
    {
        $this->render('about', ['title' => 'About']);

    }

}
