<?php

namespace Controllers;

use App\DAL\PostDAO;
use Controllers\Posts;
use \Core\Controller;
use App\Entity\User;
use App\Dal\UserDao;
use App\helpers\Helper;
use App\Services\SessionAuth;
use App\helpers\Validation;
use SplStack;


class Users extends Controller
{
    private $userDao;
    private $post;
    private $validate;
    private $postEntity;
    private $postDAO;

    public function __construct()
    {
        $this->userDao = new UserDao();
        $this->post = new Posts();
        $this->validate = new Validation();
        $this->postEntity = new \App\Entity\Post();
        $this->postDAO = new PostDAO();
    }

    public function main()
    {
        SessionAuth::requireLogIn();
        if (Helper::is_post_request()) {
            $this->post->addNewPost('/user/main');
        }
        $user = $this->userDao->find('id', $_SESSION['userId']);
        $params = [
            'title' => 'Personal',
            'user' => $user,
        ];
        $this->render('main', $params);
    }

    public function logout()
    {
        SessionAuth::logOut();
        helper::redirect('/');
    }

    public function login()
    {
        if (SessionAuth::isLoggedIn()) {
            Helper::redirect('/user/personal');
        }

        if (Helper::is_post_request()) {
            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_error' => '',
                'password_error' => '',
            ];

            $data['email_error'] = $this->validate->loginValidation($data);

            $user = new User();
            $user->setEmail($data['email']);

            $login = $this->userDao->find('email', $user->getEmail());

            $data['password_error'] = $this->validate->checkPass($login, $data);

            if (empty($data['email_error']) && empty($data['password_error'])) {
                SessionAuth::logIn($login['id']);
                Helper::redirect('/user/personal');
            }
            echo '<script language="javascript">';
            echo 'alert("' . $data['password_error'] . '\n' . $data['email_error'] . '")';
            echo '</script>';

        }
        $this->render('login', ['title' => 'Login']);
    }

    public function personal()
    {
        SessionAuth::requireLogIn();
        if (Helper::is_post_request()) {
            $this->post->addNewPost('/user/personal');
        }
        $user = $this->userDao->find('id', $_SESSION['userId']);

        $params = [
            'title' => 'Personal',
            'user' => $user,
        ];
        $this->render('personal', $params);
    }

    public function edit()
    {
        SessionAuth::requireLogIn();
        $this->render('edit', ['title' => 'Edit']);
    }

    public function signup()
    {

        if (SessionAuth::isLoggedIn()) {
            helper::redirect('/user/personal');
        }
        if (Helper::is_post_request()) {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'firstname' => trim($_POST['firstname']),
                'lastname' => trim($_POST['lastname']),
                'username' => trim($_POST['username']),
                'email' => trim($_POST['email']),
                'dob' => trim($_POST['dob']),
                'password' => trim($_POST['password']),
                'confirmPassword' => trim($_POST['confirmPassword']),
                'firstname_error' => '',
                'lastname_error' => '',
                'username_error' => '',
                'email_error' => '',
                'dob_error' => '',
                'password_error' => '',
                'confirmPassword_error' => '',
            ];

            print_r($data);

            $data['email_error'] = $this->validate->signValidation($data);
            $data['username_error'] = $this->validate->repeatCheck($data);

            if (empty($data['firstname_error']) && empty($data['lastname_error']) && empty($data['username_error']) && empty($data['email_error']) && empty($data['dob_error']) && empty($data['password_error']) && empty($data['confirmPassword_error'])) {
                $data['password'] = helper::hash($data['password'], $data['email']);
                $user = new User();
                $signup = $this->userDao->create($user->fromArray($data));
                if ($signup) {
                    SessionAuth::logIn($signup['id']);
                    Helper::redirect('/user/personal');
                }
                die('Упс, что-то пошло не так');
            }
            echo '<script language="javascript">';
            echo 'alert("' . $data['email_error'] . '\n' . $data['username_error'] . '")';
            echo '</script>';
        }
        $this->render('signup', ['title' => 'Sign up']);


    }

    public function changeInfo()
    {

        if (Helper::is_post_request()) {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'firstname' => trim($_POST['firstname']),
                'lastname' => trim($_POST['lastname']),
                'username' => trim($_POST['username']),
                'email' => trim($_POST['email']),
                'dob' => trim($_POST['dob']),
                'bio' => trim($_POST['bio']),
                'password' => trim($_POST['password']),
                'confirmPassword' => trim($_POST['confirmPassword']),
                'firstname_error' => '',
                'lastname_error' => '',
                'username_error' => '',
                'email_error' => '',
                'dob_error' => '',
                'password_error' => '',
                'confirmPassword_error' => '',
            ];

            $data['email_error'] = $this->validate->updateInfoValidation($data);
            $data['username_error'] = $this->validate->repeatCheck($data);

            if (empty($data['firstname_error']) && empty($data['lastname_error']) && empty($data['username_error']) && empty($data['email_error']) && empty($data['dob_error']) && empty($data['password_error']) && empty($data['confirmPassword_error'])) {
//                $data['password'] = helper::hash($data['password'], $data['email']);
                $user = new User();
                $updateInfo = $this->userDao->updateInfo($user->fromArray($data));
                if ($updateInfo) {
//                    SessionAuth::logIn($signup['id']);
                    Helper::redirect('/user/edit');
                }
                die('Упс, что-то пошло не так');
            }
            echo '<script language="javascript">';
            echo 'alert("' . $data['email_error'] . '\n' . $data['username_error'] . '")';
            echo '</script>';
        }
        $this->render('signup', ['title' => 'Sign up']);

    }

    public function changePassword()
    {

    }

    public function search()
    {
        SessionAuth::requireLogIn();
        $this->render('search', ['title' => 'Search']);
    }

    public function chat()
    {
        SessionAuth::requireLogIn();
        $this->render('chat', ['title' => 'Chat']);
    }


    public function username($username)
    {
        SessionAuth::requireLogIn();
        $user = $this->userDao->find('username', $username);

        $params = [
            'title' => 'Personal',
            'user' => $user,
        ];
        $this->render('personal', $params);
    }
}