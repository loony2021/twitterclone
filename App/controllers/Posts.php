<?php


namespace Controllers;


use App\DAL\PostDAO;
use App\DAL\UserDao;
use App\helpers\Helper;
use App\helpers\Validation;

class Posts
{
    private $validate;
    private $postDao;
    private $userDao;
    private $postEntity;

    public function __construct()
    {
        $this->postDao = new PostDAO();
        $this->userDao = new UserDao();
        $this->postEntity = new \App\Entity\Post();

//        $this->validate = new Validation();
    }
    public function addNewPost($uri)
    {
//        session_start();
        if (Helper::is_post_request()) {
            $data = [
                'user_id' => trim($_SESSION['userId']),
                'body' => trim($_POST['body']),
                'user_id_error' => '',
                'body_error' => ''
            ];

            $createTweet = $this->postDao->create($this->postEntity->fromArray($data));
            if ($createTweet) {
                Helper::redirect($uri);
                exit;
            }
        }
    }
    public function loadPost()
    {
        $post = $this->postDao->get($_SESSION['userId']);
        $countPost = count($post);

        for ($i = 0, $c = $countPost - 1; $i < $countPost; $i++, $c--) {
            $newPost[$i] = $post[$c];
        }

        $posts = json_encode($newPost);
        echo $posts;
    }

    public function loadAllPost()
    {
        $post = $this->postDao->getAll();
        $countPost = count($post);

        for ($i = 0, $c = $countPost - 1; $i < $countPost; $i++, $c--) {
            $newPost[$i] = $post[$c];
        }

        $posts = json_encode($newPost);
        echo $posts;
    }
    public function loadUser() {
        $users = $this->userDao->getAllUser();
        $users = json_encode($users);
        echo $users;
    }
}