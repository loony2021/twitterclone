<?php


namespace Controllers;

use \Core\Controller;

class Error extends Controller
{
    public function error404 () {
        $this->render('Errors/404', ['title' => 'Sign up']);
    }
}