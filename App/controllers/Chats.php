<?php


namespace Controllers;

use App\DAL\ChatDAO;
use App\DAL\UserDao;
use App\Entity\Chat;
use App\helpers\Helper;
use App\helpers\Validation;
use App\services\SessionAuth;
use Core\Controller;

class Chats extends Controller
{
    private $userDao;
    private $chatDao;
    private $chatEntity;

    public function __construct()
    {
        $this->userDao = new UserDao();
        $this->chatDao = new ChatDao();
        $this->chatEntity = new Chat();
    }
    public function chat($username)
    {
        SessionAuth::requireLogIn();

        $first = $_SESSION['userId'];

        $user = $this->userDao->find('username', $username);

        $second = $user['id'];

        $check = $this->chatDao->check($first, $second);

        if (!$check)
        {
            $this->chatDao->create($first, $second);
        }

        $this->render('chat', ['title' => 'Chat']);
    }
    public function getAllChats() {

        $first = $_SESSION['userId'];

        $chats = $this->chatDao->getAllConversation($first);
        $v1 = [];
        $usersId = [];
        foreach ($chats as $v1) {
            $usersId[] = $v1['first'];
            $usersId[] = $v1['second'];
        }
        do {
            unset($usersId[array_search($first, $usersId)]);
        } while (in_array($first, $usersId));
        foreach ($usersId as $item) {
            $users[] = $this->userDao->findWithoutPass('id', $item);
        }

        $users = json_encode($users);
        echo $users;
    }

    public function sendMessage() {
        if (Helper::is_post_request()) {
            $user = $this->userDao->findWithoutPass('username', $_POST['username']);

            $this->chatDao->send($conv, $_SESSION['userId'], $user['id'], $_POST['message']);
        }
    }
}