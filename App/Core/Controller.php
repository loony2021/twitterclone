<?php

namespace Core;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;
use App\Config;

class Controller
{
    public function render ($viewName, array $args = [])
    {
        static $twig = null;

        if ($twig === null) {
            $loader = new FilesystemLoader(ROOTPATH.'/Views');

            $params = array(
                'cache' => '../cache',
                'auto_reload' => true,
                'autoescape' => true,
                'debug' => true
            );
            $twig = new Environment($loader, $params);

            $asset = new TwigFunction('asset', function ($path) {
                return Config::URL.'../public/'.$path;
            });

            $title = new TwigFunction('title', function ($title = null) {
                if ($title) {
                    return $title.' - '.'Twitter clone';
                }

                return 'Twitter clone';
            });

//            $date = new TwigFunction('date', function ($original_date) {
//                return date('l, F jS Y', strtotime($original_date));
//            });
            $twig->addFunction($asset);
            $twig->addFunction($title);
        }

        try {
            echo $twig->render($viewName.'.twig', $args);
        } catch (LoaderError $e) {
            echo 'Twig LoaderError';
        } catch (RuntimeError $e) {
            echo 'Twig RuntimeError';
        } catch (SyntaxError $e) {
            echo 'Twig SyntaxError';
        }
    }
}