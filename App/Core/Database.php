<?php

namespace Core;

use PDO;
use PDOException;
use App\Config;

class Database
{
    private $host = Config::DB_HOST;
    private $user = Config::DB_USER;
    private $pass = Config::DB_PASS;
    private $dbname = Config::DB_NAME;

    public $dbh;
    private $stmt;
    private $error;

    public function __construct()
    {
        $dsn = "mysql:host=$this->host;dbname=$this->dbname";
        $option = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $option);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    public function query($sql)
    {
        $this->stmt = $this->dbh->prepare($sql);
    }

    public function bind($param, $value, $type = null)
    {
        return $this->stmt->bindParam($param, $value, $type);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function single($sql)
    {
        return $this->stmt->fetchAll();
//
    }

    public function rowCount($sql)
    {
        return count($this->stmt->fetchAll());
    }
    public function resultSet()
    {
        return $this->stmt->fetchAll();
    }
}