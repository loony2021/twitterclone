<?php

namespace Core;


use App\Config;

class Router
{

    public $errorPath = [
        'controllers\Error',
        'error404'
        ];

//    public $defaultActionName = "index";

    public function launch()
    {

        $this->resolve();

    }

    public function resolve()
    {
        if (($pos = strpos($_SERVER['REQUEST_URI'], '?')) !== false) {
            $uri = substr($_SERVER['REQUEST_URI'], 0, $pos);
        }

        $uri = $uri ?? $_SERVER['REQUEST_URI'];
        $user = $this->userPath($uri);
        if ($user)
        {
            $username= $user[1];
            $route = Config::APP_ROUTS[$user[0]];
        }
        else {
            $route = Config::APP_ROUTS[$uri];
        }
        $uri = $this->routPars($route);

        if ($uri[0] === '') {
            $controllerName = $this->errorPath[0];
            $actionName = $this->errorPath[1];
//            [$controllerName, $actionName] = $this->errorPath;
        } else {
            $controllerName = $uri[0];
            $actionName = $uri[1];
//            [$controllerName, $actionName] = $this->uri;
        }
        require_once ROOTPATH.DIRECTORY_SEPARATOR . $controllerName . '.php';
        $controller = new $controllerName;

        if (empty($user))
        {
            return $controller->$actionName();
        }
        return $controller->$actionName($username);
    }

    public function routPars($route)
    {
        $route = explode('@', $route);
        $path[0] = array_shift($route);
        $path[1] = array_shift($route);
        return $path;
    }

    public function userPath($route)
    {
        $route = explode('/', $route);
        array_shift($route);
        if (count($route) === 3)
        {
            $path[0] = $route[0].'/'.$route[1];
            $path[1] = $route[2];
            return $path;
        }
        return false;
    }

}