<?php


namespace App\Entity;


class Chat
{
    private $id;
    private $firstId;
    private $secondId;
    private $lastMessageId;
    private $message;
    private $createdAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstId()
    {
        return $this->firstId;
    }

    /**
     * @param mixed $firstId
     */
    public function setFirstId($firstId)
    {
        $this->firstId = $firstId;
    }

    /**
     * @return mixed
     */
    public function getSecondId()
    {
        return $this->secondId;
    }

    /**
     * @param mixed $secondId
     */
    public function setSecondId($secondId)
    {
        $this->secondId = $secondId;
    }

    /**
     * @return mixed
     */
    public function getLastMessageId()
    {
        return $this->lastMessageId;
    }

    /**
     * @param mixed $lastMessageId
     */
    public function setLastMessageId($lastMessageId)
    {
        $this->lastMessageId = $lastMessageId;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function fromArray(array $arrayMessage) {

    }
}