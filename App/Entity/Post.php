<?php


namespace App\Entity;


class Post
{
    private $user_id;
    private $body;
    private $like;
    private $createTime;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getLike()
    {
        return $this->like;
    }

    /**
     * @param mixed $like
     */
    public function setLike($like): void
    {
        $this->like = $like;
    }

    /**
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @param mixed $createTime
     */
    public function setCreateTime($createTime): void
    {
        $this->createTime = $createTime;
    }
    //    private $userName;
//    private $lastName;
//    private $firstName;

    /**
     * @return mixed
     */


    public function fromArray(array $arrayTweet) {
        $this->setUserId($arrayTweet['user_id']);
        $this->setBody($arrayTweet['body']);
        return $this;
    }

}
