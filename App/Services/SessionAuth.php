<?php


namespace App\services;
use App\helpers\Helper;

class SessionAuth implements Auth
{
    public static function logIn($id) {
//        $_SESSION['isLogin'] = 'true';
        $_SESSION['userId'] = $id;
        return true;
    }

    public static function logOut() {
        $_SESSION = array();
    }

    public static function requireLogIn() {
        if (static::isLoggedIn()) {
            return true;
        }
        Helper::redirect('/');
    }

    public static function isLoggedIn() {
        if ($_SESSION) {
            return true;
        }
        return false;
    }
}