<?php

namespace App\Services;

interface Auth
{
    public static function logIn($id);

    public static function logOut();

    public static function requireLogIn();

    public static function isLoggedIn();
}