<?php

namespace App;

class App
{
//    public $router;
    public static $db;
    public static $config;


    public static function init()
    {
        require 'consts.php';
        require 'Config.php';
        require 'helpers/Helper.php';
//        (new helpers\ErrorHandler())->register();
        static::bootstrap();
    }

    private static function bootstrap()
    {
        $router = new \Core\Router();
        static::$db = new \Core\Database();
        static::$config = new \App\Config();
        $router->launch();
    }
}