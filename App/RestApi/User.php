<?php


namespace RestApi;

use \Core\Controller;

class User extends Controller
{
    public function signUpUser () {
        if($this->user->signup($_POST)){
            header('Location: /user/personal');
        }
        else {
//            header('Location: /user/signup');
        }
    }
    public function loginUser () {
        if($this->user->login($_POST)){
            header('Location: /user/personal');
        }
    }
}