<?php


namespace App\helpers;


class Helper
{
    public static function is_post_request(): ?bool
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return true;
        }
        return false;
    }

    public static function redirect($uri): void
    {
        header('Location: ' . $uri);
        exit;
    }

    public static function hash($password, $salt)
    {
        return crypt($password, $salt);
    }
    public function validation($pattern, $subject){

        if (preg_match($pattern, $subject)) {
            return true;
        }
        return false;
    }
}