<?php


namespace App\helpers;


class ErrorHandler
{
    public static function getErrorName($error)
    {
        $errors = [
            E_ERROR => 'ERROR',
            E_WARNING => 'WARNING',
            E_PARSE => 'PARSE',
            E_NOTICE => 'NOTICE',
            E_CORE_ERROR => 'CORE_ERROR',
            E_CORE_WARNING => 'CORE_WARNING',
            E_COMPILE_ERROR => 'COMPILE_ERROR',
            E_COMPILE_WARNING => 'COMPILE_WARNING',
            E_USER_ERROR => 'USER_ERROR',
            E_USER_WARNING => 'USER_WARNING',
            E_USER_NOTICE => 'USER_NOTICE',
            E_STRICT => 'STRICT',
            E_RECOVERABLE_ERROR => 'RECOVERABLE_ERROR',
            E_DEPRECATED => 'DEPRECATED',
            E_USER_DEPRECATED => 'USER_DEPRECATED',
        ];
        if (array_key_exists($error, $errors)) {
            return $errors[$error] . " [$error]";
        }

        return $error;
    }

    public function register()
    {
        set_error_handler([$this, 'errorHandler']);
        register_shutdown_function([$this, 'fatalErrorHandler']);
        set_exception_handler([$this, 'exceptionHandler']);
    }

    public function errorHandler($errno, $errstr, $file, $line)
    {
        $this->showError($errno, $errstr, $file, $line);

        return true;
    }

    public function fatalErrorHandler()
    {
        if ($error = error_get_last() AND $error['type'] & (E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR)) {
//           ob_get_clean();// сбросить буфер, завершить работу буфера

            // то выводим ее в браузере
            $this->showError($error['type'], $error['message'], $error['file'], $error['line'], 500);
        }
    }

    public function exceptionHandler(\Exception $e)
    {
        $this->showError(get_class($e), $e->getMessage(), $e->getFile(), $e->getLine(), 404);
    }

    protected function showError($errno, $errstr, $file, $line, $status = 200)
    {
        header("HTTP/1.1 ($status)");

        echo $message = '<b>' . self::getErrorName($errno) . "</b><hr>" . $errstr . '<hr> file: ' . $file . '<hr> line: ' . $line . '<hr>';
        echo '<br>';
    }
}
