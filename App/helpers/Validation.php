<?php


namespace App\helpers;

use App\App;


class Validation
{
    private $db;
    private $valid;

    public function __construct()
    {
        $this->db = App::$db;
        $this->valid = new Helper;
    }

    public function repeatCheck($data)
    {
        $selectEmail = $this->db->dbh->prepare("SELECT * FROM users WHERE (email = :email)");
        $selectEmail->bindParam(':email', $data['email']);
        $selectEmail->execute();
        $resultEmail = $selectEmail->fetchAll();
        $emailCount = count($resultEmail);

        $selectUsername = $this->db->dbh->prepare("SELECT * FROM users WHERE (username = :username)");
        $selectUsername->bindParam(':username', $data['username']);
        $selectUsername->execute();
        $resultUsername = $selectUsername->fetchAll();
        $usernameCount = count($resultUsername);

        if ($emailCount === 0 && $usernameCount === 0) {
            return '';
        }
        return 'Введите корректные данные';

    }

    public function loginValidation($data)
    {
        $emailError = $this->valid->validation('/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i', $data['email']);
        $passError = $this->valid->validation('/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/', $data['password']);
        if ($emailError && $passError) {
            return '';
        }
        return 'Введены некорректные данные';
    }

    public function signValidation($data)
    {
        $emailError = $this->valid->validation('/@/', $data['email']);
        $passError = $this->valid->validation('/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/', $data['password']);
        $lastnameError = $this->valid->validation('/^[A-Za-z-]{3,30}$/', $data['lastname']);
        $firstnameError = $this->valid->validation('/^[A-Za-z-]{3,30}$/', $data['firstname']);
        $usernameError = $this->valid->validation('/^[A-Za-z-]{3,30}$/', $data['username']);
        $dobError = $this->valid->validation('/(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)/', $data['dob']);
        if ($emailError && $passError && $lastnameError && $firstnameError && $usernameError && $dobError) {
            return '';
        }
        return 'Данные введены неверно';
    }
    public function updateInfoValidation($data)
    {
        $emailError = $this->valid->validation('/@/', $data['email']);
        $lastnameError = $this->valid->validation('/^[A-Za-z-]{3,30}$/', $data['lastname']);
        $firstnameError = $this->valid->validation('/^[A-Za-z-]{3,30}$/', $data['firstname']);
        $usernameError = $this->valid->validation('/^[A-Za-z-]{3,30}$/', $data['username']);
        $dobError = $this->valid->validation('/(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)/', $data['dob']);
        if ($emailError && $lastnameError && $firstnameError && $usernameError && $dobError) {
            return '';
        }
        return 'Данные введены неверно';
    }

    public function checkPass($signup, $data)
    {
        $data['password'] = md5(Helper::hash($data['password'], $data['email']));
        if ($data['password'] === $signup['password']) {
            return '';
        }
        return 'Введен неправильный пароль';
    }


}