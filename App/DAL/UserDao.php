<?php


namespace App\DAL;


use App\App;
use App\Entity\User;
use PDO;

class UserDao
{
    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }

    public function create(User $user)
    {
        $query = 'INSERT INTO users (`firstname`, `lastname`, `email`, `username`, `dob`,`password`) VALUES (:firstname, :lastname, :email, :username, :dob, :password);';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':firstname', $user->getFirstName());
        $sth->bindParam(':lastname', $user->getLastName());
        $sth->bindParam(':email', $user->getEmail());
        $sth->bindParam(':username', $user->getUserName());
        $sth->bindParam(':dob', $user->getDob());
        $sth->bindParam(':password', md5($user->getPassword()));
        if ($sth->execute()) {
            return $this->find('email', $user->getEmail());
        }
    }

    public function find($param, $value)
    {
        $sth = $this->db->dbh->prepare('SELECT * FROM users WHERE '. $param .' = :value');
        $sth->bindParam(':value', $value);
        $sth->execute();
        $user = $sth->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            return $user;
        }
        return false;
    }

    public function findWithoutPass($param, $value)
    {
        $sth = $this->db->dbh->prepare('SELECT id, firstname, lastname, username FROM users WHERE '. $param .' = :value');
        $sth->bindParam(':value', $value);
        $sth->execute();
        $user = $sth->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            return $user;
        }
        return false;
    }

    public function getAllUser()
    {
        $sth = $this->db->dbh->prepare('SELECT id, firstname, lastname, username  FROM users');
        $sth->execute();
        $user = $sth->fetchAll();

        if ($user) {
            return $user;
        }
        return false;
    }

    public function updateInfo(User $user)
    {

        $query = 'UPDATE users SET lastname = :lastname, firstname = :firstname, email = :email, username = :username, dob = :dob WHERE id = :id;';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':firstname', $user->getFirstName());
        $sth->bindParam(':lastname', $user->getLastName());
        $sth->bindParam(':email', $user->getEmail());
        $sth->bindParam(':username', $user->getUserName());
        $sth->bindParam(':dob', $user->getDob());
        $sth->bindParam(':id', $_SESSION['userId']);
//        $sth->bindParam(':password', md5($user->getPassword()));
        if ($sth->execute()) {
            return true;
        }
    }

    public function update(User $user)
    {

    }

    public function delete(int $userId)
    {

    }
}