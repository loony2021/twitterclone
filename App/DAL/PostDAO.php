<?php


namespace App\DAL;

use App\App;
use App\Entity\Post;
use PDO;


class PostDAO
{
    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }

    public function create(Post $tweet)
    {
         $query = 'INSERT INTO posts (`user_id`,`body`, `created_at`) VALUES (:user_id, :body, current_timestamp());';
        $sth = $this->db->dbh->prepare($query);
        $user_id = $tweet->getUserId();
        $tweet_body = $tweet->getBody();
        $sth->bindParam(':user_id', $user_id);
        $sth->bindParam(':body', $tweet_body);
        if ($sth->execute()) {
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function get($id)
    {
        $post = 'SELECT * FROM posts WHERE user_id = :user_id';
        $sth = $this->db->dbh->prepare($post);
        $sth->bindParam(':user_id', $id);
        $sth->execute();
        $post = $sth->fetchAll();

        return $post;
    }
    public function getAll()
    {
        $post = 'SELECT * FROM posts';
        $sth = $this->db->dbh->prepare($post);
        $sth->execute();
        $post = $sth->fetchAll();

        return $post;
    }
}