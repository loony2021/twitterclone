<?php


namespace App\DAL;

use App\App;
use PDO;

class ChatDAO
{
    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }
    public function create($first, $second)
    {
        $query = 'INSERT INTO conversation (`first`,`second`) VALUES (:first, :second);';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':first', $first);
        $sth->bindParam(':second', $second);
        if ($sth->execute()) {
            return true;
        }
        return false;
    }

    public function check($first, $second) {
        $sth = $this->db->dbh->prepare('SELECT * FROM conversation WHERE first LIKE :first AND second LIKE :second');
        $sth->bindParam(':first', $first);
        $sth->bindParam(':second', $second);
        $sth->execute();
        $my = $sth->fetch(PDO::FETCH_ASSOC);

        if ($my) {
            return $my;
        }

        $sth = $this->db->dbh->prepare('SELECT * FROM conversation WHERE first LIKE :second AND second LIKE :first');
        $sth->bindParam(':first', $first);
        $sth->bindParam(':second', $second);
        $sth->execute();
        $your = $sth->fetch(PDO::FETCH_ASSOC);

        if ($your) {
            return $your;
        }

        return false;
    }

    public function getAllConversation($id)
    {
        $sth = $this->db->dbh->prepare('Select * from conversation where first  LIKE :id  OR second Like :id');
        $sth->bindParam(':id', $id);
        $sth->execute();
        return $sth->fetchAll();
    }

    public function findConversation($userId) {
        $query = 'SELECT * FROM `conversation` WHERE `first` = 117 AND `second` = 124';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':first', $first);
        $sth->bindParam(':second', $second);
    }

    public function send($conv_id, $sender, $adressee, $message)
    {
        $query = 'INSERT INTO messages (conv_id, sender, addressee, message) VALUES (:conv_id, :sender, :adressee, :message)';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':conv_id', $conv_id);
        $sth->bindParam(':sender', $sender);
        $sth->bindParam(':adressee', $adressee);
        $sth->bindParam(':message', $message);
        $sth->execute();
        return $sth->fetchAll();
    }
}