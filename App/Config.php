<?php

namespace App;

class Config
{
    public const  URL = '/';
    public const  NAME = 'Dev Communication';
    public const  DB_TYPE = 'mysql';
    public const  DB_HOST = 'localhost';
    public const  DB_NAME = 'twitter';
    public const  DB_USER = 'wizard';
    public const  DB_PASS = 'wizard';
    public const  SHOW_ERRORS = true;
    public const  APP_ROUTS = [
        '/' => 'controllers\Home@index',
        '/about' => 'controllers\Home@about',
        '/user/login' => 'controllers\Users@login',
        '/user/personal' => 'controllers\Users@personal',
        '/user/edit' => 'controllers\Users@edit',
        '/user/search' => 'controllers\Users@search',
        '/user/main' => 'controllers\Users@main',
        '/user/signup' => 'controllers\Users@signup',
        'user/username' => 'controllers\Users@username',
        'user/chat' => 'controllers\Chats@chat',

        '/api/signup' => 'controllers\Users@signup',
        '/api/login' => 'controllers\Users@login',
        '/api/logout' => 'controllers\Users@logout',
        '/api/loadPost' => 'controllers\Posts@loadPost',
        '/api/loadUser' => 'controllers\Posts@loadUser',
        '/api/loadAllPost' => 'controllers\Posts@loadAllPost',
        '/api/loadAllChats' => 'controllers\Chats@getAllChats',
        '/api/sendMessage' => 'controllers\Chats@sendMessage',
        '/api/changePassword' => 'controllers\Users@changePassword',
        '/api/changeInfo' => 'controllers\Users@changeInfo',
//        'api/user/personal' => 'controllers\User@personal',
//        'api/user/edit' => 'controllers\User@edit',
//        'api/user/search' => 'controllers\User@search',
//        'api/user/main' => 'controllers\User@main',


    ];

}