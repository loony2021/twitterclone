"use strict"

const editFormInfo = document.getElementById('edit-form-info');
const editFormPassword = document.getElementById('edit-form-password');
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
const email = document.getElementById('email');
const userName = document.getElementById('username');
const bio = document.getElementById('bio');
const dateOfBirth = document.getElementById('dateOfBirth');
const currentPassword = document.getElementById('currentPassword');
const password = document.getElementById('password');
const confirmPassword = document.getElementById('confirmPassword');
const changePassword = document.getElementById('change-pass');
const updateBtn = document.getElementById('submitBtn');
let error = true;
changePassword.addEventListener('click', updatePassword);
updateBtn.addEventListener('click', updateInfo);

const passwordEye = document.getElementById('password-eye');
const confirmPasswordEye = document.getElementById('confirm-password-eye');
const currentPasswordEye = document.getElementById('current-password-eye');

passwordEye.addEventListener('click', passwordType);
function passwordType() {

    if ( password.type == 'password') {
        password.type = 'text';
    } else {
        password.type = 'password';
    }
}

confirmPasswordEye.addEventListener('click', confirmPasswordType);
function confirmPasswordType() {

    if ( confirmPassword.type == 'password') {
        confirmPassword.type = 'text';
    } else {
        confirmPassword.type = 'password';
    }
}

currentPasswordEye.addEventListener('click', currentPasswordType);
function currentPasswordType() {

    if ( currentPassword.type == 'password') {
        currentPassword.type = 'text';
    } else {
        currentPassword.type = 'password';
    }
}


function updatePassword() {
    error = true;
    if (password.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
        password.style.border = "0";
        password.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        password.style.border = "0";
        password.style.borderBottom = "2px solid red";
    }

    if (currentPassword.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g) && currentPassword != password) {
        currentPassword.style.border = "0";
        currentPassword.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        currentPassword.style.border = "0";
        currentPassword.style.borderBottom = "2px solid red";
    }

    if (confirmPassword.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g) && confirmPassword == password) {

        confirmPassword.style.border = "0";
        confirmPassword.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        confirmPassword.style.border = "0";
        confirmPassword.style.borderBottom = "2px solid red";
    }


    if (error) {
        editFormPassword.action = '/api/changePassword';
        editFormPassword.method = 'POST';
        editFormPassword.submit();
    }
}

function updateInfo() {
    error = true;

    if (firstName.value.match(/^[A-Za-z-]{3,30}$/)) {
        firstName.style.border = "0";
        firstName.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        firstName.style.border = "0";
        firstName.style.borderBottom = "2px solid red";
    }

    if (lastName.value.match(/^[A-Za-z-]{3,30}$/)) {
        lastName.style.border = "0";
        lastName.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        lastName.style.border = "0";
        lastName.style.borderBottom = "2px solid red";
    }

    if (email.value.match(/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i)) {
        email.style.border = "0";
        email.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        email.style.border = "0";
        email.style.borderBottom = "2px solid red";
    }
    if (userName.value.match(/^[A-Za-z-]{3,30}$/)) {
        userName.style.border = "0";
        userName.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        userName.style.border = "0";
        userName.style.borderBottom = "2px solid red";
    }

    if (bio.value.match(/([А-Яа-яA-Za-z0-9_\-!@#$%^&*]{0,1000})/)) {
        bio.style.border = "0";
        bio.style.borderBottom = "2px solid #DEE2E6";
    } else {
        error = false;
        bio.style.border = "0";
        bio.style.borderBottom = "2px solid red";
    }

    if (error) {
        editFormInfo.action = '/api/changeInfo';
        editFormInfo.method = 'POST';
        editFormInfo.submit();
    }

}