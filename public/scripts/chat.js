const chatsBlock = document.getElementById('chats');
const massageBlock = document.getElementById('message');
const textMessage = document.getElementById('textMessage');

const sendBtn = document.getElementById('submit');

async function loadChats() {
    let users = await fetch('/api/loadAllChats');
    users = await users.json();
    renderChats(users);
}

function renderChats(users) {
    users.forEach((element) => {
        chatsBlock.innerHTML += "<a href=\"" + element['username'] + "\"><li class=\"contact\">\n" +
            "                       <div class=\"wrap\">\n" +
            "                           <img src=\"http://emilcarlsson.se/assets/louislitt.png\" alt=\"\" />\n" +
            "                           <div class=\"meta\">\n" +
            "                               <p class=\"name\">" + element['firstname'] + " " + element['lastname'] + " @" + element['username'] + "</p>\n" +
            // "                               <p class=\"preview\">You just got LITT up, Mike.</p>\n" +
            "                           </div>\n" +
            "                       </div>\n" +
            "                   </li>";
    })
}

loadChats();

sendBtn.addEventListener('click', checkMessage);

let error = true;
function checkMessage() {
    if( textMessage.value.match(/([А-Яа-яA-Za-z0-9_\-!@#$%^&*]{3,140})/)) {
        error = true;
        textMessage.style.border = "0";
        textMessage.style.borderBottom = "2px solid #DEE2E6";
        // textareaDanger.style.display = "none";

    } else {
        error = false;
        textMessage.style.border = "1px solid red";
        // textareaDanger.style.display = "block";
    }
    if (error) {
        sendMessage(textMessage.value);
    }
}

function sendMessage(message) {
    let uri = window.location.pathname;
    let username = uri.split('/')[3];
    $.post(
        "/api/sendMessage",
        {
            message: message,
            username: username

        },
        onAjaxSuccess
    );

    function onAjaxSuccess(data)
    {
        // Здесь мы получаем данные, отправленные сервером и выводим их на экран.
        alert(data);
    }
}
