(function($) {
    $(function() {

        $('ul.personal-tabs__caption').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div')
            $(".tabs-content-block").find('div.personal-tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

    });
})(jQuery);


const addTwittBtn = document.getElementById('submitBtn');
const addTwitt = document.getElementById('textarea');
const textareaDanger = document.getElementById('textarea-danger');
const signUpForm = document.getElementById('postForm');
const postsBlock = document.getElementById('posts');
addTwittBtn.addEventListener('click', checkInput);

async function loadPost() {
    let posts = await fetch('/api/loadPost');
    posts = await posts.json();
    let users = await fetch('/api/loadUser');
    users = await users.json();
    renderPost(posts, users);
}

function renderPost(posts, users) {
    let user = {};
    users.forEach((element) => {
        user[element['id']] = {
            lastname: element['lastname'],
            firstname: element['firstname'],
            username: element['username'],
        }
    });
    for (let key in posts) {
        let userId = posts[key]['user_id'];
        postsBlock.innerHTML += "<div class=\"card main-page-card mb-2 mt-4 person-card main-page-person-card\">\n" +
            "    <div class=\"card-body py-3 px-0 \">\n" +
            "\n" +
            "        <div class=\"person-info-block main-page-person-info-block\">\n" +
            "            <img class=\"person-img main-page-person-img mx-3\" src=\"../../../public/images/tape/giulioBXProfilePic.png\">\n" +
            "            <span class=\"px-1\">" + user[userId]['lastname'] + " " + user[userId]['firstname'] +" @" + user[userId]['username'] +"</span>\n" +
            "            <span class=\"card-subtitle tweet-time main-page-tweet-time px-3 text-muted font-size-12\">" + posts[key]['created_at'] + "</span>\n" +
            "        </div>\n" +
            "\n" +
            "        <p class=\"card-text mb-3 px-3 font-weight-normal main-page-card-title card-title\">" + posts[key]['body'] + "</p>\n" +
            "\n" +
            "        <div class=\"mt-2 px-3\">\n" +
            "            <div class=\"tweet-footer main-page-tweet-footer\">\n" +
            "\n" +
            "                <div class=\"mr-2 \">\n" +
            "                    <i class=\"fas fa-heart\"></i>\n" +
            "                    <span class=\" font-weight-bold font-size-12\">150</span>\n" +
            "                </div>\n" +
            "\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>";
    }
}

loadPost();

let error = true;
function checkInput() {
    if( addTwitt.value.match(/([А-Яа-яA-Za-z0-9_\-!@#$%^&*]{3,140})/)) {
        error = true;
        addTwitt.style.border = "0";
        addTwitt.style.borderBottom = "2px solid #DEE2E6";
        textareaDanger.style.display = "none";

    } else {
        error = false;
        addTwitt.style.border = "1px solid red";
        textareaDanger.style.display = "block";
    }
    if (error) {
        signUpForm.action = '/user/personal';
        signUpForm.method = 'POST';
        signUpForm.submit();
    }
}



function count() {

    let txt = document.getElementById('textarea').value; //вытаскиваеем текст
     txt = txt.replace(/ {1,}/gi, ""); // удаляем пробелы (заменяем ничем)
    let cnt = txt.length;	// считаем длинну
    document.getElementById('count-symbol').value = cnt;  // выводим

}





