'use strict';

const submitBtn = document.getElementById('submitBtn');
const email = document.getElementById('email');
const password = document.getElementById('password');
const loginForm = document.getElementById('loginForm');
const emailDanger = document.getElementById('email-danger');
const passDanger = document.getElementById('pass-danger');

submitBtn.addEventListener('click', login);

let error;

function login () {
    error = false;
    if (email.value.match(/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i)) {
        email.style.border = "0";
        email.style.borderBottom = "2px solid #DEE2E6";
        emailDanger.style.display = "none";

    } else {
        error = true;
        email.style.borderBottom = "2px solid red";
        emailDanger.style.display = "block";
    }
    if (password.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
        password.style.border = "0";
        password.style.borderBottom = "2px solid #DEE2E6";
        passDanger.style.display = "none";
    } else {
        error = true;
        password.style.borderBottom = "2px solid red";
        passDanger.style.display = "block";
    }
    if (!error) {
        loginForm.action = '/api/login';
        loginForm.method = 'POST';
        loginForm.submit();
    }
}