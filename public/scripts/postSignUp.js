'use strict';

const submitBtn = document.getElementById('submitBtn');
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
const email = document.getElementById('email');
const userNames = document.getElementById('username');
const dob = document.getElementById('dob');
const password = document.getElementById('password');
const confirmPassword = document.getElementById('confirmPassword');
const signUpForm = document.getElementById('signUpForm');

const emailDanger = document.getElementById('email-danger');
const firstNameDanger = document.getElementById('firstName-danger');
const lastNameDanger = document.getElementById('lastName-danger');
const userNameDanger = document.getElementById('username-danger');
const passDanger = document.getElementById('pass-danger');
const confirmPasswordDanger = document.getElementById('confirmPassword-danger');
const dateOfBirthDanger = document.getElementById('dateOfBirth-danger');
const passwordEye = document.getElementById('password-eye');
const confirmPasswordEye = document.getElementById('confirm-password-eye');

submitBtn.addEventListener('click', signUp);



passwordEye.addEventListener('click', passwordType);
function passwordType() {

    if ( password.type == 'password') {
        password.type = 'text';
    } else {
        password.type = 'password';
    }
}

confirmPasswordEye.addEventListener('click', confirmPasswordType);
function confirmPasswordType() {

    if ( confirmPassword.type == 'password') {
        confirmPassword.type = 'text';
    } else {
        confirmPassword.type = 'password';
    }
}





let error;

function signUp() {
    error = false;
    if (firstName.value.match(/^[A-Za-z-]{3,30}$/)) {
        firstName.style.border = "0";
        firstName.style.borderBottom = "2px solid #DEE2E6";
        firstNameDanger.style.display = "none";
    } else {
        error = true;
        firstName.style.borderBottom = "2px solid red";
        firstNameDanger.style.display = "block";
    }
    if (lastName.value.match(/^[A-Za-z-]{3,30}$/)) {
        lastName.style.border = "0";
        lastName.style.borderBottom = "2px solid #DEE2E6";
        lastNameDanger.style.display = "none";
    } else {
        error = true;
        lastName.style.borderBottom = "2px solid red";
        lastNameDanger.style.display = "block";
    }
    if (email.value.match(/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i)) {
        email.style.border = "0";
        email.style.borderBottom = "2px solid #DEE2E6";
        emailDanger.style.display = "none";

    } else {
        error = true;
        email.style.borderBottom = "2px solid red";
        emailDanger.style.display = "block";
    }
    if (userNames.value.match(/^[A-Za-z-]{3,30}$/)) {
        userNames.style.border = "0";
        userNames.style.borderBottom = "2px solid #DEE2E6";
        userNameDanger.style.display = "none";
    } else {
        error = true;
        userNames.style.borderBottom = "2px solid red";
        userNameDanger.style.display = "block";
    }

    if (dob.value.match(/(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)/)) {
        dob.style.border = "0";
        dob.style.borderBottom = "2px solid #DEE2E6";
        dateOfBirthDanger.style.display = "none";
    } else {
        error = true;
        dob.style.borderBottom = "2px solid red";
        dateOfBirthDanger.style.display = "block";
    }

    if (password.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
        password.style.border = "0";
        password.style.borderBottom = "2px solid #DEE2E6";
        passDanger.style.display = "none";
    } else {
        error = true;
        password.style.borderBottom = "2px solid red";
        passDanger.style.display = "block";
    }

    if (confirmPassword.value === password.value && confirmPassword.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
        confirmPassword.style.border = "0";
        confirmPassword.style.borderBottom = "2px solid #DEE2E6";
        confirmPasswordDanger.style.display = "none";
    } else {
        error = true;
        confirmPassword.style.borderBottom = "2px solid red";
        confirmPasswordDanger.style.display = "block";
    }
    if (!error) {
        signUpForm.action = '/api/signup';
        signUpForm.method = 'POST';
        signUpForm.submit();
    }
}